queue()
    .defer(d3.json, "/donorschoose/projects")
    .defer(d3.json, "static/geojson/india_states.geojson")
    .await(makeGraphs);

function makeGraphs(error, projectsJson, statesJson) {

	var censusProject = projectsJson;
	var ndx = crossfilter(censusProject);

	// Total Population Count
	var n = ndx.groupAll().reduceSum(function(d) { if(d.Age == "All ages"){ return d.TotalPersons; } else{ return 0; } })
	var totalPopulation = dc.numberDisplay("#totalPopulationCount");
        totalPopulation
                .formatNumber(d3.format("d"))
                .valueAccessor(function(d){return d; })
                .group(n)
                .formatNumber(d3.format(".3s"));

	// Rural Population Count
        var m = ndx.groupAll().reduceSum(function(d) { if(d.Age == "All ages"){ return d.RuralPersons; } else{ return 0; } })
        var ruralPopulation = dc.numberDisplay("#ruralPopulationCount");
        ruralPopulation
                .formatNumber(d3.format("d"))
                .valueAccessor(function(d){return d; })
                .group(m)
                .formatNumber(d3.format(".3s"));

        // Urban Population Count
        var mm = ndx.groupAll().reduceSum(function(d) { if(d.Age == "All ages"){ return d.UrbanPeople; } else{ return 0; } })
        var urbanPopulation = dc.numberDisplay("#urbanPopulationCount");
        urbanPopulation
                .formatNumber(d3.format("d"))
                .valueAccessor(function(d){return d; })
                .group(mm)
                .formatNumber(d3.format(".3s"));

        // Male Population Count
        var male = ndx.groupAll().reduceSum(function(d) { if(d.Age == "All ages"){ return d.TotalM; } else{ return 0; } })
        var malePopulation = dc.numberDisplay("#malePopulationCount");
        malePopulation
                .formatNumber(d3.format("d"))
                .valueAccessor(function(d){return d; })
                .group(male)
                .formatNumber(d3.format(".3s"));

        // Female Population Count
        var female = ndx.groupAll().reduceSum(function(d) { if(d.Age == "All ages"){ return d.TotalF; } else{ return 0; } })
        var femalePopulation = dc.numberDisplay("#femalePopulationCount");
        femalePopulation
                .formatNumber(d3.format("d"))
                .valueAccessor(function(d){return d; })
                .group(female)
                .formatNumber(d3.format(".3s"));

	// State-wise Distribution
	var stateDim = ndx.dimension(function(d) { return d["AreaName"]; });
	var totalByState = stateDim.group().reduceSum(function(d) { if(d.Age == "All ages"){ return d.TotalPersons; } else{ return 0; } });
	var stateWiseTypeChart = dc.rowChart("#in-state-distribution");
	stateWiseTypeChart
        	.width(670)
        	.height(850)
        	.dimension(stateDim)
        	.group(totalByState)
		.ordering(function(d){ return -d.value })
		.elasticX(1)
        	.xAxis().ticks(10);

        // State-wise Distribution
        var stateDim = ndx.dimension(function(d) { return d["Age"]; });
        var totalByState = stateDim.group().reduceSum(function(d) { if(d.Age != "All ages"){ return d.TotalPersons; } else{ return 0; } });
        var stateWiseTypeChart = dc.lineChart("#distributionByAge");
        stateWiseTypeChart
                .width(670)
                .height(400)
                .dimension(stateDim)
                .group(totalByState)
                .ordering(function(d){ return -d.value })
                .elasticX(1)
		.elasticY(1)
		.yAxisLabel("Number of people")
		.xAxisLabel("Age")
		.x(d3.scale.linear())
		.margins().left += 40;


    // Render the graphs
    dc.renderAll();
	
};
